export const topbar = () => {
  const topbar = document.querySelector('.topbar')
  const html = document.documentElement

  if (topbar.classList.contains('is-fixed')) {
    html.classList.add('has-fixed-topbar')
  }
}
